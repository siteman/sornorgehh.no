<?php
/**
 * JointsWP-child Theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package JointsWP-child
 */

add_action( 'wp_enqueue_scripts', 'JointsWP_master_parent_theme_enqueue_styles' );

/**
 * Enqueue scripts and styles.
 */
function JointsWP_master_parent_theme_enqueue_styles() {
	wp_enqueue_style( 'JointsWP-master-style', get_template_directory_uri() . '/style.css' );
	wp_enqueue_style( 'JointsWP-child-style',
		get_stylesheet_directory_uri() . '/style.css',
		array( 'JointsWP-master-style' )
	);

}
