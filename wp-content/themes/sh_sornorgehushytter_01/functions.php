<?php
/**
 * Elementor Hello Theme functions and definitions
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( ! isset( $content_width ) ) {
	$content_width = 800; // pixels
}

/*
 * Set up theme support
 */
if ( ! function_exists( 'elementor_hello_theme_setup' ) ) {
	function elementor_hello_theme_setup() {
		if ( apply_filters( 'elementor_hello_theme_load_textdomain', true ) ) {
			load_theme_textdomain( 'elementor-hello-theme', get_template_directory() . '/languages' );
		}

		if ( apply_filters( 'elementor_hello_theme_register_menus', true ) ) {
			register_nav_menus( array( 'menu-1' => __( 'Primary', 'elementor-hello-theme' ) ) );
		}

		if ( apply_filters( 'elementor_hello_theme_add_theme_support', true ) ) {
			add_theme_support( 'post-thumbnails' );
			add_theme_support( 'automatic-feed-links' );
			add_theme_support( 'title-tag' );
			add_theme_support( 'custom-logo' );
			add_theme_support( 'html5', array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
			) );
			add_theme_support( 'custom-logo', array(
				'height' => 100,
				'width' => 350,
				'flex-height' => true,
				'flex-width' => true,
			) );

			/*
			 * WooCommerce:
			 */
			if ( apply_filters( 'elementor_hello_theme_add_woocommerce_support', true ) ) {
				// WooCommerce in general:
				add_theme_support( 'woocommerce' );
				// Enabling WooCommerce product gallery features (are off by default since WC 3.0.0):
				// zoom:
				add_theme_support( 'wc-product-gallery-zoom' );
				// lightbox:
				add_theme_support( 'wc-product-gallery-lightbox' );
				// swipe:
				add_theme_support( 'wc-product-gallery-slider' );
			}
		}
	}
}
add_action( 'after_setup_theme', 'elementor_hello_theme_setup' );

/*
 * Theme Scripts & Styles
 */
if ( ! function_exists( 'elementor_hello_theme_scripts_styles' ) ) {
	function elementor_hello_theme_scripts_styles() {
		if ( apply_filters( 'elementor_hello_theme_enqueue_style', true ) ) {
			wp_enqueue_style( 'elementor-hello-theme-style', get_stylesheet_uri() );
		}
	}
}
add_action( 'wp_enqueue_scripts', 'elementor_hello_theme_scripts_styles' );

/*
 * Register Elementor Locations
 */
if ( ! function_exists( 'elementor_hello_theme_register_elementor_locations' ) ) {
	function elementor_hello_theme_register_elementor_locations( $elementor_theme_manager ) {
		if ( apply_filters( 'elementor_hello_theme_register_elementor_locations', true ) ) {
			$elementor_theme_manager->register_all_core_location();
		}
	}
}
add_action( 'elementor/theme/register_locations', 'elementor_hello_theme_register_elementor_locations' );



// disable for posts
add_filter('use_block_editor_for_post', '__return_false', 10);


function displayTodaysDate( $atts ) {
	//return date(get_option('date_format'));
	//return the_time( get_option( 'date_format' ) );
	return date('Y');
}
add_shortcode( 'sh_dagensdato', 'displayTodaysDate');


// Hent ut ACF Taxonomy Område Shortcode
function sh_acf_omrade( $atts ) {
	//Vars
	$term = get_field('sh_omrade');
	
	if( $term ):
		return $term->name;
	endif; 
}
add_shortcode( 'sh_omrade_acf', 'sh_acf_omrade');


// Hent ut telefonnummer og fjern mellomrom Shortcode
function sh_acf_cleanphone( $atts ) {
	//Vars
	$term = get_field('sh_telefon');
	$term_clean = preg_replace("/[^0-9]/", "", $term);
	
	return 'tel:' . $term_clean;
}
add_shortcode( 'sh_cleanphone', 'sh_acf_cleanphone');


// Lage mailto-link Shortcode
function sh_acf_email( $atts ) {
	//Vars
	$term = get_field('sh_epost');
	
	return 'mailto:' . $term;
}
add_shortcode( 'sh_mailto', 'sh_acf_email');


// Legge til FacetWP i Elementor Custom Query, Query ID = sh_facetwp
add_action( 'elementor_pro/posts/query/sh_facetwp', function( $query ) {
	// Modify the query
	$query->set( 'facetwp', true );
} );


// Hente ut underentreprenører i områder
add_action( 'elementor_pro/posts/query/sh_entrepreneur', function( $query ) {
	
	global $post;
	// Vars
	$sh_omrade = get_field('sh_omrade', $post->ID);

	// Add our tax query
	$tax_query[] = [
		'taxonomy' => 'sted',
		'field' => 'term_id',
		'terms' => array( $sh_omrade->term_id ),
	];

	// Modify the query
	$query->set( 'tax_query', $tax_query );

} );


